def afficherPopulations(noMois,nbchoux,nbchevres,nbloups):
    print(" fin du mois #"+str(noMois)+" => nb de choux: " + str(nbchoux) + " / nb de chèvres: "+str(nbchevres)+" / nb de loups: "+str(nbloups))

#---main---
    
#conditions initiales
nbChoux = 250000
nbChevres = 1000
nbLoups = 100
cmEauParMois = 30

cptMois = 1
while cptMois <= 12: #chaque
    nbChoux += cmEauParMois*700 #Le nombre de choux augmente chaque mois (5 choux par litres d'eau tombée)
    if nbChevres*15 > nbChoux: #s'il n'y a pas assez de choux pour toutes les chèvres
        nbChevres = int(nbChevres*0.8) #la population des chèvres décline de 20%
        nbChoux = 0 #les chèvres ont donc mangé tous les choux existants
    else: #sinon (il y suffisamment de choux pour les chèvres)
        nbChoux -= nbChevres*15 #chaque chèvre mange 15 choux
        nbChevres = int(nbChevres*1.2) #les chèvres augmentent donc leur population de 20% (car elles ont assez mangé)
    #considérant le nombres de chèvres...
    if nbLoups > nbChevres: #s'il n'y a pas assez de chèvres pour tous les loups
        nbLoups = int(nbLoups*0.7) #la population des loups décline de 30%
        nbChevres = 0 #et les loups ont donc mangé toutes les chèvres
    else: #sinon (il y a suffisamment de chèvres pour tous les loups)
        nbChevres -= nbLoups #chaque loup mange une chèvre
        nbLoups = int(nbLoups*1.3) # ils augmentent donc leur population de 30% (car ils ont assez mangé)
        
    #affichage du mois courant:
    afficherPopulations(cptMois,nbChoux,nbChevres,nbLoups)
    
    #passage au mois suivant (pour le prochain tour de boucle si applicable)
    cptMois +=1