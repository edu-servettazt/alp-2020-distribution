from gturtle import *

def carre40():
    forward(40)
    right(90)
    forward(40)
    right(90)
    forward(40)
    right(90)
    forward(40)
    right(90)

def triangle40():
    forward(40)
    right(135)
    forward(40 * 1.414)
    right(135)
    forward(40)
    right(90)

def poisson():
    left(135)
    triangle40()
    right(180)
    carre40()
    left(45)

def montre_poisson():
    setPenColor("red")
    poisson()
    setPenColor("white")
    delay(700)
    poisson();

def decale_poisson_droite():
    montre_poisson()
    right(90)    
    fd(40*1.414+5)
    left(90)
    print("x: "+str(getTurtle().getPos().getX())+" y:"+str(getTurtle().getPos().getX()))

def decale_poisson_gauche():
    montre_poisson()
    left(90)    
    fd(40*1.414+5)
    right(90)
    print("x: "+str(getTurtle().getPos().getX())+" y:"+str(getTurtle().getPos().getX()))

def decale_poisson_haut():
    montre_poisson()       
    fd(40*1.414+5)    
    print("x: "+str(getTurtle().getPos().getX())+" y:"+str(getTurtle().getPos().getX()))

def decale_poisson_bas():
    montre_poisson()
    right(180)    
    fd(40*1.414+5)    
    left(180)
    print("x: "+str(getTurtle().getPos().getX())+" y:"+str(getTurtle().getPos().getX()))

def va_a_droite():
    decale_poisson_droite()
    decale_poisson_droite()
    decale_poisson_droite()
    decale_poisson_droite()
    decale_poisson_droite()
    decale_poisson_droite()

def va_a_gauche():
    decale_poisson_gauche()
    decale_poisson_gauche()
    decale_poisson_gauche()
    decale_poisson_gauche()
    decale_poisson_gauche()
    decale_poisson_gauche()

def va_en_haut():
    decale_poisson_haut()
    decale_poisson_haut()
    decale_poisson_haut()
    decale_poisson_haut()
    decale_poisson_haut()
    decale_poisson_haut()
    
def va_en_bas():
    decale_poisson_bas()
    decale_poisson_bas()
    decale_poisson_bas()
    decale_poisson_bas()
    decale_poisson_bas()
    decale_poisson_bas()
#--main--
makeTurtle()
hideTurtle()
va_a_gauche()
va_a_droite()
va_en_haut()
va_en_bas()

