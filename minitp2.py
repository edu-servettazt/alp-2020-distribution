#liste: une liste de nombres,
#pivot: la valeur frontière,
def compteNbValeursAvantApres(liste,pivot):
    #todo: retourne une liste de 2 valeurs (nb avant, nb après)
    pp_pg = []
    nbpp = 0
    nbpg = 0
    for el in liste:
        if el > pivot:
            nbpg+=1
        elif el < pivot:
            nbpp+=1
    pp_pg.append(nbpp)
    pp_pg.append(nbpg)
    return pp_pg

def somme(liste):
    somme = 0
    for el in liste:
        somme += el
    return somme

def nbPivotsDansListe(liste,pivot):
    cpt=0
    for el in liste:
        if el == pivot:
            cpt+=1
    return cpt

#listeBase: la liste complète
#listeDecompte: la liste des 2 valeurs (nb avant, nb après)
def verifieResultat(listeBase, listeDecompte, pivot):
    #todo: #retourne vrai le compte est bon (toutes les valeurs on été comptées),
            #retourne faux sinon
    return somme(listeDecompte) == len(listeBase)-nbPivotsDansListe(listeBase,pivot)
#    equivalent à: 
#    if somme(listeDecompte) == len(listeBase)-nbPivotsDansListe(listeBase,pivot):
#        return True
#    else:
#        return False
    
#retounr une chaine de caractère contenant les éléments de la liste séparée par le caractère sep (dans des [])
def listeEnChaine(liste,sep):
    cpt = 1
    chaine = "["
    for el in liste:
        chaine+=str(el)
        if cpt < len(liste):
            chaine+=sep
        cpt+=1
    chaine+="]"
    return chaine

#listeBase: la liste complète
#listeDecompte: la liste des 2 valeurs (nb avant, nb après)
def afficherResultat(listeComplete, listeDecompte, pivot):
    #todo: affiche par exemple:
    #"la liste [1,2,3,4,5,6,7,8,9,10] contient 3 nombres avant la valeur pivot 4 et 6 après"
    print("la liste "+listeEnChaine(listeComplete,",")+" contient "+str(listeDecompte[0])+" nombres avant la valeur pivot "+str(pivot)+" et "+str(listeDecompte[1])+" après")

def verifieEtAffiche(lc, ld, p):
    if verifieResultat(lc, ld, p):
        afficherResultat(lc, ld, p)
    else:
        print("problème: le nombre d'éléments décomptés ne correspond pas au nombre d'éléments dans la liste (pivot(s) exclu(s))")

#--main--
liste1 = [1,2,3,4,5,6,7,8,9,10]
liste2 = [0,1,7,1,5,9,2,6,5,3,34,0,23,11,75,23,55,4,12,10,4,67,99]
liste3 = [1000,32,-10,0,9012,234,642,9,-9,-10,98,22,-33]
#todo:
#afficher le résultat pour les 3 listes si le compte est bon, sinon afficher "problème"
verifieEtAffiche(liste1,compteNbValeursAvantApres(liste1,4),4)
verifieEtAffiche(liste2,compteNbValeursAvantApres(liste2,4),4)
verifieEtAffiche(liste3,compteNbValeursAvantApres(liste3,4),4)

