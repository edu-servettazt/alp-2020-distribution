#Thomas Servettaz | 16.09.2020
from gturtle import *

def cercle(perimetre, couleur):
    setPenColor(couleur)
    repeat 180:
        left(360/180)
        forward(perimetre/180)

#--main
makeTurtle()
hideTurtle()
cercle(180, "blue")