#Thomas Servettaz | 16.09.2020
from gturtle import *

def cercle(perimetre, couleur):
    setPenColor(couleur)
    repeat 180:
        left(360/180)
        forward(perimetre/180)

def cercle_double(perimetre, couleur):    
    cercle(perimetre, couleur)
    cercle(perimetre/2, couleur)    

#--main
makeTurtle()
hideTurtle()
cercle_double(180, "blue")
cercle_double(180, "red")
cercle_double(90, "green")