#Thomas Servettaz | 16.09.2020
from gturtle import *
from math import pi

def cercle(perimetre, couleur):
    setPenColor(couleur)
    repeat 180:
        left(360/180)
        forward(perimetre/180)

def cercle_double(perimetre, couleur):    
    cercle(perimetre, couleur)
    cercle(perimetre/2, couleur)    

def next_position(distance):
    penUp()
    right(90)
    fd(distance)
    left(90)
    penDown()

#--main
makeTurtle()
hideTurtle()
cercle_double(180, "blue")
next_position(180/pi+20)
cercle_double(180, "red")
next_position(90/pi+20)
cercle_double(90, "green")