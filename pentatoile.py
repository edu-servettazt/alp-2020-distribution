from gturtle import *
from math import sqrt

def tourne_et_marque(taille_point, couleur_base, couleur_point, angle_droite):
    setPenColor(couleur_point)
    rt(angle_droite)
    dot(taille_point)
    setPenColor(couleur_base)    

def etoile(taille_cote_penta,couleur):
    rt(36/2)
    setPenColor(couleur)    
    repeat 5:        
        fd(taille_cote_penta)
        #rt(180-36)
        tourne_et_marque(taille_cote_penta/10, couleur, "yellow", 180-36)
    lt(18)

def penta(taille_branche_etoile, couleur):
    rt(90)
    setPenColor(couleur)
    repeat 5:                
        fd(taille_branche_etoile/((1+sqrt(5))/2))
        #lt(360/5)
        tourne_et_marque(taille_branche_etoile/7, couleur, "red", -360/5)
    lt(90)

def pentatoile(taille, couleur_1, couleur_2):   
    penta(taille, couleur_2)
    etoile(taille, couleur_1)    

#--main--
makeTurtle()
#pentatoile(200, "green", "blue")
hideTurtle()
repeat 15:
    pentatoile(150, "green", "blue")
    penUp()
    rt(360)
    bk(60)
    penDown()
    
