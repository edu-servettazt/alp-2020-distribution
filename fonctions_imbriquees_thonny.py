from turtle import *

def a(x):
    forward(x)
    right(4.5*x)
    b(x+3,20)

def b(x,y):
    forward(y)
    c(x)
    back(y*3)

def c(y):
    left(90)
    forward(20)
    right(90)
    back(y*2)

#--main--
a(20)