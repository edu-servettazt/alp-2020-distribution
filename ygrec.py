# Chapitre 2 - Auto-éval
# Ex 3
from gturtle import *

def ligne_et_retour():
    forward(10)
    back(10)

def ygrec():
    forward(10)
    left(30)
    ligne_et_retour()
    right(60)
    ligne_et_retour()
    left(30)
    back(10)
      
def double_ygrec():
    forward(20)
    left(30)
    ygrec()
    right(60)
    ygrec()
    left(30)
    back(20)
    
def quadruple_ygrec():
    forward(40)
    left(30)
    double_ygrec()
    right(60)
    double_ygrec()
    left(30)
    back(40)

makeTurtle()
quadruple_ygrec()