def premier_de_la_liste(liste):
    if len(liste) > 0:
        return liste[0]
    else:
        return None


def dernier_de_la_liste(liste):
    if len(liste) > 0:
        return liste[-1]
    else:
        return None


def avant_dernier_de_la_liste(liste):
    if len(liste) >= 2:
        return liste[-2]
    else:
        return None


def nieme_valeur_de_la_liste(liste, n):
    if n <= len(liste) and n >= 1:
        return liste[n-1]
    else:
        return None


# --main--
liste_1 = [-3, -1, 0, 1, 1, 4, 6, -1, 7, 8]  # liste de 10 nombres entiers
liste_2 = ["alice", "bob"]  # liste de deux chaînes de caractères
liste_3 = [-3.5]  # liste de un nombre réel
liste_4 = []  # liste vide

print("************** CORRIGE NAIF **************")

# premiers:
premier_liste_1 = premier_de_la_liste(liste_1)
print('le premier de la liste [-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] est:')
print('==>' + str(premier_liste_1))

premier_liste_2 = premier_de_la_liste(liste_2)
print('le premier de la liste ["alice", "bob"] est:')
print('==>' + str(premier_liste_2))

premier_liste_3 = premier_de_la_liste(liste_3)
print('le premier de la liste [-3.5] est:')
print('==>' + str(premier_liste_3))

premier_liste_4 = premier_de_la_liste(liste_4)
print('le premier de la liste [] est:')
print('==>' + str(premier_liste_4))

# derniers:
dernier_liste_1 = dernier_de_la_liste(liste_1)
print('le dernier de la liste [-3, -1, 0, 1, 1, 4, 6, -1, 7, 8] est:')
print('==>' + str(dernier_liste_1))

dernier_liste_2 = dernier_de_la_liste(liste_2)
print('le dernier de la liste ["alice", "bob"] est:')
print('==>' + str(dernier_liste_2))

dernier_liste_3 = dernier_de_la_liste(liste_3)
print('le dernier de la liste [-3.5] est:')
print('==>' + str(dernier_liste_3))

dernier_liste_4 = dernier_de_la_liste(liste_4)
print('le dernier de la liste [] est:')
print('==>' + str(dernier_liste_4))

# avant-derniers:
# todo...trop long :( --> voir plus bas :)

# nièmes:
# todo...trop long :( --> voir plus bas :)

print("************** FIN DU CORRIGE NAIF **************")
print("") # pour sauter une ligne
print("*************** CORRIGE INTERMEDIAIRE ***********")

def afficher_liste(liste,sep):
    """Converti une liste en une String (chaque élément séparé par le caractère sep)"""
    liste_string='['
    cpt=1
    for el in liste:
        liste_string+=str(el)
        if cpt<len(liste):
            liste_string+=sep
        cpt+=1
    liste_string+=']'
    return liste_string

def afficherResultats(liste, resultat, texte):
    print(texte + " de la liste " + afficher_liste(liste,',') + " est: ")
    print('==> ' +str(resultat))

afficherResultats(liste_1, premier_de_la_liste(liste_1), "le premier")
afficherResultats(liste_1, dernier_de_la_liste(liste_1), "le dernier")
afficherResultats(liste_1, avant_dernier_de_la_liste(liste_1), "l'avant-dernier")
afficherResultats(liste_1, nieme_valeur_de_la_liste(liste_1,3), "le troisieme")

afficherResultats(liste_2, premier_de_la_liste(liste_2), "le premier")
afficherResultats(liste_2, dernier_de_la_liste(liste_2), "le dernier")
afficherResultats(liste_2, avant_dernier_de_la_liste(liste_2), "l'avant-dernier")
afficherResultats(liste_2, nieme_valeur_de_la_liste(liste_2,3), "le troisieme")

afficherResultats(liste_3, premier_de_la_liste(liste_3), "le premier")
afficherResultats(liste_3, dernier_de_la_liste(liste_3), "le dernier")
afficherResultats(liste_3, avant_dernier_de_la_liste(liste_3), "l'avant-dernier")
afficherResultats(liste_3, nieme_valeur_de_la_liste(liste_3,3), "le troisieme")

afficherResultats(liste_4, premier_de_la_liste(liste_4), "le premier")
afficherResultats(liste_4, dernier_de_la_liste(liste_4), "le dernier")
afficherResultats(liste_4, avant_dernier_de_la_liste(liste_4), "l'avant-dernier")
afficherResultats(liste_4, nieme_valeur_de_la_liste(liste_4,3), "le troisieme")


print("*************** FIN DU CORRIGE INTERMEDIAIRE ***********")
print("") # pour sauter une ligne
print("*************** CORRIGE OPTIMAL ***********")

liste_de_listes = [liste_1,liste_2,liste_3,liste_4] #on crée une liste contenant les listes à traiter
for liste in liste_de_listes: #pour chaque liste contenue dans la liste de listes:
    print('--------------------------------------------------------------------') # plus joli
    afficherResultats(liste, premier_de_la_liste(liste), "le premier")
    afficherResultats(liste, dernier_de_la_liste(liste), "le dernier")
    afficherResultats(liste, avant_dernier_de_la_liste(liste), "l'avant-dernier")
    afficherResultats(liste, nieme_valeur_de_la_liste(liste,3), "le troisieme")

print("*************** FIN DU CORRIGE OPTIMAL ***********")

#todo: modifier le code du corrigé naïf pour éviter un maximum de répétitions
