#Auteur: Thomas Servettaz
from gturtle import *
from math import sqrt

#dessine un carre de 100 par 100 en partant du coin NW, regarde en haut à la fin
def dessine_carre():
    rt(90)
    fd(100)
    rt(90)
    fd(100)
    rt(90)
    fd(100)
    rt(90)
    fd(100)

#dessine un triangle équilatéral de 100 de côté en partant du coin SW et regarde en haut à la fin
def dessine_triangle():
    rt(30)
    fd(100)
    rt(120)
    fd(100)
    rt(120)
    fd(100) 
    rt(90) # regarde en l'air!
    
#dessine une croix (un noeud papillon)
def dessine_croix():
    rt(135)
    fd(sqrt(100**2 + 100**2))
    lt(135)
    fd(100)
    lt(135)
    fd(sqrt(100**2 + 100**2))
    rt(135)
    fd(100)

#déplace la tortue à droite de 120 pas sans dessiner, regarde en haut au début et à la fin
def deplace_a_droite():
    penUp()
    rt(90)
    fd(120)
    lt(90)
    penDown()

#dessine une maison complète avec un croix, tortue au coin NW du carré à la fin (tête en haut)
def dessine_maison():
    dessine_triangle()
    dessine_carre()
    dessine_croix()

#---main---
makeTurtle()
setPenWidth(3)
setPenColor("red")
hideTurtle()
dessine_maison()
deplace_a_droite()
dessine_maison()
deplace_a_droite()
dessine_maison()