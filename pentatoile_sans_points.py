#Thomas Servettaz
from gturtle import *
from math import sqrt 

def etoile(taille_cote_penta,couleur):
    rt(36/2)
    setPenColor(couleur)    
    repeat 5:        
        fd(taille_cote_penta)
        rt(180-36)        
    lt(18)

def penta(taille_branche_etoile, couleur):
    rt(90)
    setPenColor(couleur)
    repeat 5:                
        fd(taille_branche_etoile/((1+sqrt(5))/2))
        lt(360/5)        
    lt(90)

def pentatoile(taille, couleur_1, couleur_2):   
    penta(taille, couleur_2)
    etoile(taille, couleur_1)    

#--main--
makeTurtle()
pentatoile(200, "green", "blue")
hideTurtle()