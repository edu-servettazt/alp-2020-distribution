from gturtle import *

def grand_cercle():
    repeat 180:
        forward(300/180)
        rt(2)

def demi_cercle_haut():
    repeat 45:
        forward((150/2)/45)
        rt(4)

def demi_cercle_bas():
    repeat 45:
        forward((150/2)/45)
        lt(4)

#--main--
makeTurtle()
hideTurtle()
grand_cercle()
demi_cercle_haut()
demi_cercle_bas()